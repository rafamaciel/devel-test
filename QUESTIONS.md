## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
 - Web Design Responsivo: Páginas adaptáveis para todos os dispositivos.
- Dominando JavaScript com jQuery.

2. Quais foram os últimos dois framework/CMS que você trabalhou?
Os últimos dois frameworks que eu trabalhei foram Symfony e Zend.

3. Descreva os principais pontos positivos do seu framework favorito.
O Symfony 2 possui arquitetura modular, permite a abstração de dados, é bem documentado e possuí inúmeros bundles, o que facilita bastante no desenvolvimento.
 
4. Descreva os principais pontos negativos do seu framework favorito.
Apesar da documentação ser bem completa o Symfony é um framework difícil aprendizagem (Acredito que por ser tão robusto).

5. O que é código de qualidade para você.
Um código de qualidade para mim é o que é desenvolvido utilizando boas práticas de programação, sendo o mais limpo possível e visando sempre obter o melhor desempenho possível.

## Conhecimento Linux

1. O que é software livre?
Software livre é qualquer programa que tenha seu código aberto, podendo ser usado, copiado, modificado e distribuído livremente. Seus usuários podem ter acesso ao código fonte e alterar conforme desejar (Seguindo os termos de sua licença). 

2. Qual o seu sistema operacional favorito?
Meu sistema operacional favorito é o Linux, distribuição Fedora Core.

3. Já trabalhou com Linux ou outro Unix-like?
Sim. Atualmente utlizo Fedora Core. Mas eu também já trabalhei com as distribuições Ubuntu, Slackware e Debian e Red Hat.

4. O que é SSH?
SSH é um protocolo que permite acessar virtualmente um servidor como se fosse em um terminal em que toda a transmissão dos dados é criptografada, sendo necessário apenas se conectar (na gande maioria das vezes através da porta 22).

5. Quais as principais diferenças entre sistemas *nix e o Windows?
As principais diferenças entre o Windows e o Linux são: que o Windows possui um sistema completamente codificado para que não possa ser editado. Já o Linux possui o código fonte totalmente aberto, permitindo a sua modificação, adição ou remoção de aplicações ou que você mesmo crie aplicações para ele.  
Além disso, o Linux é muito mais seguro e estável que o Windows. Com um sistema que só permite a instalação de algum pacote com a permissão do usuário da máquina, o que não acontece com o Windows em que muitas vezes ao buscar um software para instalação ele vem com outros de "brinde" que muitas vezes são instalados sem a permissão do usuário.  
O Linux também possui uma comunidade muito mais responsiva do que a Microsoft, sendo assim é muito raro você fazer uma pergunta em algum fórum e não ser respondido. Já com o Windows você na maioria dos problemas mais sérios você só pode contar com o suporte help desk pago.  


## Conhecimento de desenvolvimento

1. O que é GIT?
É um sistema de controle de versão e de gerenciamento de código, muito eficaz para controlar descentralizadamente o andamento de um projeto e também para trabalhar em equipe com facilidade.

2. Descreva um workflow simples de trabalho utilizando GIT.
 - Realizar Fork no repositório desejado;
 - Criar um branch para ralização das alterações;
-  Realizar o commit das alterações realizadas;
-  Efetuar check-out; caso necessário.

3. O que é PHP Data Objects?
É um módulo de PHP criado para prover uma padronização da forma com que PHP se comunica com um banco de dados relacional.

4. O que é Database Abstract Layer?
É uma interface de programação de aplicativo que unifica a comunicação entre uma aplicação e uma base de dados.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
É um conjunto de classes que permite armazenar e recuperar informações em um banco de dados relacional sem precisar escrever muitos códigos de conexão com o banco de dados e query's de SQL. Dessa forma fica mais fácil dar manutenção em um projeto e também melhora a padronização da aplicação, sem contar que com o ORM diminui você escreve menos código, conseguindo ser mais produtivo.

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
Não sou um expert no assunto mas procuro compreender seu paradigmas cada vez mais para melhorar o conteúdo dos meus trabalhos.
7. O que é Dependency Injection?
É um padrão de desenvolvimento para manter baixo o nível de acoplamento entre diferentes módulos da aplicação, em que as dependências entre os módulos são definidas pela configuração de uma infraestrutura de software responsável por injetar em cada componente suas dependências declaradas.

8. O significa a sigla S.O.L.I.D?
O SOLID são técnicas de programação orientada a objetos sob a junção de 5 princípios:
Single Responsibility Principle: princípio da responsabilidade única;
Open Closed Principle: princípio do aberto/fechado;
Liskov Substitution Principle: princípio da substituição de Liskov;
Interface Segregation Principle: princípio da segregação de Interfaces;
Dependency Inversion Principle: princípio da inversão de dependência.

9. Qual a finalidade do framework PHPUnit?
A finalidade do PHPUnit é antecipar a descoberta de erros, seja de digitação ou lógica, durante o desenvolvimento de aplicações agilizando a correção dos mesmos. 

10. Explique o que é MVC.
É um padrão de arquitetura de software que separa informação da interface com a qual o usuário interage. Sendo uma forma de estruturar a aplicação de forma que a interface de interação esteja separada do controle da informação, sendo intermediada por outra camada controladora.
