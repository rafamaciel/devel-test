<?php
/**
 * Realiza a inicialização da aplicação.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @uses Rafamaciel\Framework\Http\Request para tratar as requisições.
 */

// Adiciona o autoload gerado pelo composer.
require "..".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";
use Rafamaciel\Framework\Http\Request;
// Cria o método para obter a requisição.
$request = new Request();
// Processa a requisição.
$request->proccess();
