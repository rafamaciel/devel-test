<?php

namespace Rafamaciel\Crud\Controller;

use Rafamaciel\Framework\Controller\Controller;
use Rafamaciel\Framework\Http\Request as Request;
use Rafamaciel\Crud\Model\Article as Article;

/**
 * Controller do CRUD
 *
 * @package Crud
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @uses Rafamaciel\Framework\Http\Request Para obter as requisições.
 * @uses Rafamaciel\Crud\Model\Article Entidade usada pelas views.
 */
class DefaultController extends Controller
{


    /**
     * Index.
     */
    public function indexAction()
    {
        $model = new Article();
        $this->template->models = $this->database->entity($model)->findAll()->getResult();
        $this->template->render('index');
    }

    /**
     * Adiciona novos artigos.
     */
    public function addAction()
    {
        $model = new Article();
        $request = new Request();
        if ($request->getMethod()== 'GET') {
            $this->template->render('add');
        } else {
            $params = (object)$_POST;
            $model->setTitle($params->title);
            $model->setSlug($params->slug);
            $model->setDescription($params->description);
            $model->setBody($params->body);
            $model->setAuthor($params->author);
            $model->setInsert_date(date('Y-m-d'));
            $model->setUpdate_date(date('Y-m-d'));
            $this->database->entity($model)->add();
            $model = null;
            $model = new Article();
            $this->template->models = $this->database->entity($model)->findAll()->getResult();
            $this->template->render('index');
        }
    }

    /**
     * Exibe o post selecionado.
     */
    public function showAction()
    {
        $model = new Article();
        $request = new Request();
        $attrs = $request->getArgs();
        $this->template->model = (object)$this->database->entity($model)->findById($attrs[0])->getResult();
        $this->template->render('show');
    }

    /**
     * Remove artigos.
     */
    public function deleteAction()
    {
        $model = new Article();
        $request = new Request();
        $attrs = $request->getArgs();
        $this->template->model = (object)$this->database->entity($model)->delete($attrs[0]);
        header('Location: /');
    }

    /**
     * 
     */
    public function editAction()
    {
        $model = new Article();
        $request = new Request();
        $id = $request->getArgs();
        $id = $id[0];
        if ($request->getMethod()== 'GET') {
            $attrs = $request->getArgs();
            $this->template->model = (object)$this->database->entity($model)->findById($attrs[0])->getResult();
            $this->template->render('edit');
        } else {
            $params = (object)$_POST;
            $model->setTitle($params->title);
            $model->setSlug($params->slug);
            $model->setDescription($params->description);
            $model->setBody($params->body);
            $model->setAuthor($params->author);
            $model->setUpdate_date(date('Y-m-d'));
            $this->database->entity($model)->update($id);
            header('Location: /');
        }
    }
}
