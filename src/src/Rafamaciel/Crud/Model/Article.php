<?php

namespace Rafamaciel\Crud\Model;

use Rafamaciel\Framework\Model\Model as BaseModel;

/**
 * Base do Controller
 *
 * @package Crud
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class Article extends BaseModel
{

    /**
     * @var String $title Titulo do artigo.
     */
    private $title;

    /**
     * Get title
     *
     * @return String Titulo do artigo.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param String $tile Titulo do artigo.
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @var String $slug Slug do artigo.
     */
    private $slug;

    /**
     * Get slug
     *
     * @return String Slug do artigo.
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param String $tile Slug do artigo.
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @var Text $description Descrição do artigo.
     */
    private $description;

    /**
     * Get description
     *
     * @return String Descrição do artigo.
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param String $description description do artigo.
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @var Text $body Corpo do artigo.
     */
    private $body;

    /**
     * Get body
     *
     * @return String Corpo do artigo.
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set body
     *
     * @param String $body Corpo do artigo.
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @var String $author Autor do artigo.
     */
    private $author;

    /**
     * Get author
     *
     * @return String Autor do artigo.
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author
     *
     * @param String $author Autor do artigo.
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @var Datetime $insert_date Data de criação do artigo.
     */
    private $insert_date;

    /**
     * Get insert_date
     *
     * @return Datetime Data de publicação do artigo.
     */
    public function getInsert_date()
    {
        return $this->insert_date;
    }

    /**
     * Set insert_date
     *
     * @param Datetime $insert_date Data de publicação do artigo.
     */
    public function setInsert_date($insert_date)
    {
        $this->insert_date = $insert_date;
    }

    /**
     * @var Datetime $update_date Data de update do artigo.
     */
    private $update_date;

    /**
     * Get update_date
     *
     * @return Datetime Data de publicação do artigo.
     */
    public function getUpdate_date()
    {
        return $this->update_date;
    }

    /**
     * Set update_date
     *
     * @param Datetime $update_date Data de publicação do artigo.
     */
    public function setUpdate_date($update_date)
    {
        $this->update_date = $update_date;
    }
}
