<?php

namespace Rafamaciel\Framework\Controller;

use Rafamaciel\Framework\Http\Request;
use Rafamaciel\Framework\Template\TemplateEngine;
use Rafamaciel\Framework\Core as core;

/**
 * Base do Controller
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @uses Rafamaciel\Framework\Core Para obter o database manager.
 * @uses Rafamaciel\Framework\Template\TemplateEngine Para renderizar os templates.
 */
class Controller
{


    /**
     * @var Rafamaciel\Framework\Database\Database $database Armazena o database manager.
     */
    protected $database;

    /**
     * Contrutor da classe
     * Instancia as varáveis necessárias para o correto funcionamento da classe.
     */
    public function __construct()
    {
        $this->database = core::databaseManager();
        $this->template = new TemplateEngine($this->modulePath() . DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR);
    }

    /**
     * Obtem o caminho do modulo.
     *
     * @return String Caminho do modulo.
     */
    protected function modulePath()
    {
        $path = 'root\..\src\className';
        $path = str_replace('root', $_SERVER['DOCUMENT_ROOT'], $path);
        $path = str_replace('className', get_class($this), $path);
        $path = explode('\\', $path);
        array_pop($path);
        array_pop($path);
        $path = implode(DIRECTORY_SEPARATOR, $path);
        return $path;
    }
}
