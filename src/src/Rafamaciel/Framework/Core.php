<?php

namespace Rafamaciel\Framework;

use Rafamaciel\Framework\Yaml\Yaml;
use Rafamaciel\Framework\Database\Database;
use Rafamaciel\Framework\Database\DataManager;

/**
 * Nucleo da applicação.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class Core
{

    /**
     * @var Array $conf Recebe as informações contidas no arquivo de configuração do framework.
     */
    private static $conf;

    /**
     * Inicializador da classe.
     */
    public static function init()
    {
        // Realiza a leitura do arquivo de configuração
        $conf = new Yaml($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR ."/config.yml");
        // Instancia a variavel de configurações.
        self::$conf = $conf->get();
    }

    /**
     * Retorna o conteudo da configuração desejada.
     *
     * @param String $attr Configuração desejada.
     */
    public static function get($attr)
    {
        return self::$conf[$attr];
    }

    /**
     * Cria o database manager.
     *
     * @return Rafamaciel\Framework\Database\DataManager Novo database manager.
     */
    public static function databaseManager()
    {
        // Cria um database.
        $database = new Database(self::$conf['database']);
        // Abre a conexão.
        $database->open();
        // Retorna o database manager.
        return new DataManager($database->adapter());
    }
}
Core::init();
