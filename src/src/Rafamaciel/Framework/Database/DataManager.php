<?php

namespace Rafamaciel\Framework\Database;

use \ReflectionClass;
use \ReflectionObject;
use \ReflectionMethods;
use \Exception;

/**
 * Realiza a conexão e as operações no banco de dados.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class DataManager
{
    /**
     * @var Rafamaciel\Framework\Model\Model $model Modelo da classe.
     */
    private $model = null;

    /**
     * @var \ReflectionProperty $reflection Reflexo da classe.
     */
    private $reflection;

    /**
     * @var Rafamaciel\Framework\Database\DatabaseAdapter.
     */
    private $adapter;

    /**
     * Instancia o modelo que será utilizado.
     *
     * @param Rafamaciel\Framework\Model\Model $model Objeto Modelo.
     *
     * @uses \ReflectionClass Para obter o reflexo da classe.
     *
     * @return Rafamaciel\Framework\Database\DataManager Retorna a própria classe.
     */
    public function entity($model)
    {
        // Reflete o modelo desejado.
        $this->reflection = new ReflectionClass($model);
        // instancia o modelo.
        $this->model = $model;
        // Retorna.
        return $this;
    }

    /**
     * Adiciona uma nova entidade no banco de dados.
     *
     * @uses \Exception Para retornar as mensagens de erro.
     *
     * @throws Exception Caso nenhuma entidade tenha sido definida.
     */
    public function add()
    {
        // Verfica se a entidade foi definida.
        if ($this->model == null) {
            // Retorna mensagem de erro caso não tenha sido definida.
            throw new Exception('Nenhuma entidade foi selecionada.');
        } else {
            // Retorna o resultado da insersão no banco de dados.
            return $this->newEntity($this->reflectObj());
        }
    }

    /**
     * Obtem a lista de propriedade privadas da entidade.
     *
     * @uses \ReflectionProperty Para refletir os objetos privados da classe.
     *
     * @return Array Lista contendo as propriedades da entidade.
     */
    private function reflectObj()
    {
        // Instancia o array que receberá as variaveis da classe.
        $result = array();
        // Obtem o reflexo da classe.
        $obj = $this->reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
        // Varre o reflexo extraindo os nomes das variaveis privadas.
        foreach ($obj as $key) {
            // Adiciona o nome no array.
            array_push($result, $key->name);
        }
        // Retorna o resultado.
        return $result;
    }

    /**
     * Cria um novo objeto pra ser inserido no banco de dados.
     *
     * @param Array $objs Objetos que serão inseridos.
     */
    private function newEntity($objs)
    {
        // Instancia o array que irá guardar os metodos.
        $insert = array();
        // Varre a lista de objetos.
        foreach ($objs as $key => $value) {
            // Gera o nome do metodo de recuperação do objeto.
            $method = 'get' . ucfirst($value);
            // Verifica se o metodo de recuperação existe.
            if (method_exists($this->model, $method)) {
                // Recupera o valor e adiciona na lista de recuperação.
                array_push($insert, array('column'=>$value, 'value'=>$this->model->$method()));
            }
        }
        return $this->adapter->insert($this->model->getTable(), $insert)->execute();
    }

    /**
     * Trata os métodos dinamicos
     *
     * @param string $function Nome do método requisitado.
     * @param mixed[] $params Parametros passados para este método.
     *
     * @return Rafamaciel\Framework\Database\DataManager Retorna a própria classe.
     */
    public function __call($function, $params)
    {
        // Verifica se o método requisitado é um findBy (exemplo findByName)
        if (strstr($function, 'findBy')==true) {
            // Obtem a propriedade que será consultada.
            $propriety = strtolower(str_replace('findBy', '', $function));
            // Executa a consulta via findBy.
            return $this->findBy($propriety, $params[0]);
        }
        if (strstr($function, 'delete')==true) {
            // Obtem a propriedade que será consultada.
            $propriety = 'id';
            // Executa a consulta via findBy.
            return $this->removeById($propriety, $params[0]);
        }
    }

    /**
     * Obtem uma entidade usando um de seus objetos com chave.
     *
     * @param String $propriety Recebe a propriedade que será consultada.
     * @param Mixed[] $value Recebe o valor que esta propriedade deve conter.
     *
     * @return Array resultado da consulta.
     */
    public function findBy($propriety, $value)
    {
        //Recebe a propriedade desejada e seu respectivo valor.
        $arg = array($propriety=>$value);
        // Retorna o resultado da consulta.
        return $this->adapter->select($this->model->getTable(), $arg);
    }

    /**
     * Remove uma entidade pelo id.
     *
     * @param String $propriety Recebe a propriedade que será excluida.
     * @param String $value Recebe o valor que a propriedade deve conter.
     *
     * @return Array|boolean Resultado da exclusão.
     */
    public function removeById($propriety, $value)
    {
        // Instancia  os argumentos da query de exclusão.
        $arg = array($propriety=>$value);
        // Retorna o resultado da exclusão.
        return $this->adapter->delete($this->model->getTable(), $arg);
    }

    /**
     * Busca todas as entidades de uma classe
     *
     * @return Aray Array contendo o resultado da conseulta.
     */
    public function findAll()
    {
        // Retorna o resultado da consulta.
        return $this->adapter->select($this->model->getTable());
    }

    /**
     * Realiza o update dos dados de uma entidade.
     *
     * @param integer $id Id da entidade.
     *
     * @return Mixed[] Resultado do update.
     */
    public function update($id)
    {
        // Reflete o objeto.
        $objs = $this->reflectObj();
        // Instancia o array que receberá os objetos.
        $update = array();
        // Varre a lista de objetos.
        foreach ($objs as $key => $value) {
            // Gera o nome do metodo de recuperação do objeto.
            $method = 'get' . ucfirst($value);
            // Verifica se o metodo de recuperação existe.
            if (method_exists($this->model, $method) and $method != 'getInsert_date') {
                // Recupera o valor e adiciona na lista de recuperação.
                array_push($update, array('column'=>$value, 'value'=>$this->model->$method()));
            }
        }
        // Retorna o resultado do update.
        return $this->adapter->update($this->model->getTable(), $update, $id)->execute();
    }

    /**
     * Construtor da classe.
     *
     * @param Rafamaciel\Framework\Database\DataAdapter $adapter Objeto do adaptador.
     */
    public function __construct($adapter)
    {
        // Instancia o adaptador do banco de dados.
        $this->adapter = $adapter;
    }
}
