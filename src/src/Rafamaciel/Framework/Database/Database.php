<?php

namespace Rafamaciel\Framework\Database;

use \PDO;
use \Exception;

/**
 * Obtem o objeto de manipulação do banco de dados.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br>
 *
 * @uses \PDO Para manipular a conexão com o banco de dados.
 */
class Database
{
    /**
     * @var PDO $connection Armazena a conexão com o banco de dados
     */
    private $connection;

    /**
     * @var Array $config Armazena os dados de conexão com bando dados.
     */
    private $config;

    /**
     * Construtor da classe
     *
     * @param Array $params Dados de conexão com o banco de dados (hostname, database, username, password).
     */
    public function __construct($params)
    {
        // Armazena os dados de conexão.
        $this->config = $params;
    }

    /**
     * Abre a conexão com o banco de dados.
     *
     * @throws Exception Erro ao criar o objeto PDO.
     */
    public function open()
    {
        try {
            // Cria uma nova conexão usando PDO.
            $this->connection = new PDO('mysql:host='.$this->config['hostname'].';dbname='.$this->config['database'].'', $this->config['username'], $this->config['password']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            // Retorna mensagem de erro.
            throw $e;
        }
    }

    /**
     * Obtem o adaptador.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Adaptador.
     */
    public function adapter()
    {
        return new DatabaseAdapter($this->connection);
    }
}
