<?php

namespace Rafamaciel\Framework\Database;

use \PDO;
use \Exception;
use \InvalidArgumentException;
use \RuntimeException;

/**
 * Realiza as operações no banco de dados MySql.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @uses \PDO Para manipulação do banco de dados.
 * @uses \Exception Para gerar as mensagens de erro.
 * @uses \Rafamaciel\Framework\Database\QueryProvider Para gerar as querys de consulta no MySql.
 */
class DatabaseAdapter
{
    /**
     * @var PDO OBjectt $connection Armazena o objeto de conexão com o banco de dados.
     */
    private $connection;

    /**
     * @var Rafamaciel\Framework\Database\QueryProvider $qp Armazena o objeto provedor de querys.
     */
    private $qp;

    /**
     * @var Mixed[] $args Armazena os argumentos obtidos na construção da query.
     */
    private $args = null;

    /**
     * @var Mixed[] $result Armazena o resultado da consulta.
     */
    private $result;

    /**
     * @var Integer $rowsAfected Armazena as linha afetadas pela consulta.
     */
    private $rowsAfected;

    /**
     * Construtor da classe.
     *
     * @param \PDO $connection Objeto para manipulação do banco de dados.
     */
    public function __construct($connection)
    {
        // Instancia o Query Provider.
        $this->qp = new QueryProvider();
        // Instancia a conexão com o banco de dados.
        $this->connection = $connection;
    }

    /**
     * Rotina que prepara a inserção de objetos.
     *
     * @param String $table Nome da tabela.
     * @param Array $params Dados que serão inseridos.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Retorna a propria classe.
     */
    public function insert($table, $params)
    {
        // Obtem a query de inserção.
        $this->args = $this->qp->insert($table, $params);
        // Retorna a própria classe.
        return $this;
    }

    /**
     * Realiza consultas no banco de dados
     *
     * @param String $table Nome da tabela.
     * @param Array|null $params Parametros que deverão ser consultados.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Retorna a propria classe.
     */
    public function select($table, $params = null)
    {
        // Verifica se o parametro é nulo
        if ($params == null) {
            // Caso seja, gera a query que seleciona todos os itens.
            $this->args = $this->qp->all($table);
            // Executa a query e armazena o resultado.
            $this->result = $this->connection->query($this->args['query'])->fetchAll();
        } else {
            // Caso os parametros da consulta tenham sido recebidos.
            $this->args = $this->qp->select($table, $params);
            // Cria define a query de conxão.
            $query = $this->args['query'];
            // Varre a query...
            foreach ($this->args['values'] as $key => $value) {
                // ... e reescreve os valores necessários.
                $query = str_replace($key, $value, $query);
            }
            // Executa a query e armazena o resultado.
            $this->result = $this->connection->query($query)->fetch(PDO::FETCH_ASSOC);
        }
        // Retorna a propria classe.
        return $this;
    }

    /**
     * Deleta uma entidade.
     *
     * @param String $table Nome da tabela.
     * @param String $params Paramatros necessários para a exclusão.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Retorna a propria classe.
     */
    public function delete($table, $params)
    {
        // Obtem a query de inserção.
        $this->args = $this->qp->delete($table, $params);
        // Prepara a query.
        $stmt = $this->connection->prepare($this->args['query']);
        // Varre os argumentos...
        foreach ($this->args['values'] as $key => $value) {
            // Adicionando os valores da consulta.
            $stmt->bindParam($key, $value);
        }
        // Executa a exclusão.
        $this->result = $stmt->execute();
        // Retorna a própria classe.
        return $this;
    }

    /**
     * Executa a consulta no banco de dados.
     *
     * @return Mixed[] Resultado da consulta.
     * @throws PDOException Erro do php.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Retorna a propria classe.
     */
    public function execute()
    {
        try {
            // Prepara a query de conexão com o banco de dados.
            $stmt = $this->connection->prepare($this->args['query']);
            // Executa a query e armazena os resultados.
            if ($this->args['values'] != null) {
                $this->result = $stmt->execute($this->args['values']);
            } else {
                $this->result = $stmt->execute();
            }
            // Armazena as linhas afetadas
            $this->rowsAfected = $stmt->rowCount();
        } catch (PDOException $e) {
            // Gera exception.
            echo 'Error: ' . $e->getMessage();
        }
        // Retorna a própria classe.
        return $this;
    }

    /**
     * Realiza update de uma entidade.
     *
     * @param String $table Nome da tabela.
     * @param Array $params Dados para o update
     * @param Integer $id Id do objeto que recebera o update.
     *
     * @return Rafamaciel\Framework\Database\DatabaseAdapter Retorna a propria classe.
     */
    public function update($table, $params, $id)
    {
        // Obtem a query de inserção.
        $this->args = $this->qp->update($table, $params, $id);
        // Retorna a própria classe.
        return $this;
    }

    /**
     * Obtem as linhas afetadas pela consulta
     *
     * @return Integer Linhas afetadas.
     */
    public function getRows()
    {
        // Retorna as linhas afetadas.
        return $this->rowsAfected;
    }

    /**
     * Obtem o resultado da operação realizada.
     *
     * @return Mixed[] Resultado da operação realizada.
     */
    public function getResult()
    {
        return $this->result;
    }
}
