<?php

namespace Rafamaciel\Framework\Database;

/**
 * Classe utilizada para a construção de querys de consulta no MySql.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class QueryProvider
{
    /**
     * Cria query de inserção de conteudo
     *
     * @param String $table Nome da tabela onde o conteudo será inserido.
     * @param Array $params Dados a serem inseridos na tabela.
     *
     * @return Array Array contendo a query de inserção e os dados a serem inseridos.
     */
    public function insert($table, $params)
    {
        //Cria array para receber os parametros de entrada da query.
        $objs = array();
        // Cria array para receber os valores que serão executados.
        $values = array();
        // Varre os parametros recebidos.
        foreach ($params as $obj) {
            // Adciciona o parametro atual à lista de parametros da query.
            array_push($objs, ":".$obj['column']);
            $values[":".$obj['column']] = $obj['value'];
        }
        // Implode o array de parametros.
        $objs = implode($objs, ', ');
        // Cria a query.
        $query = "INSERT INTO ".$table."(".str_replace(':', '', $objs).") VALUES(".$objs.")";
        // Retorna a query e os dados necessários para executala.
        return array('query'=>$query, 'values'=>$values);
    }

    /**
     * Cria query para selecionar todos os itens de uma tabela.
     *
     * @param String $table Nome da tabela.
     *
     * @return String Query de consulta.
     */
    public function all($table)
    {
        $query = 'SELECT * FROM '. $table;
        return array('query' => $query, 'values'=>null);
    }

    /**
     * Cria query para selecionar um determinado item.
     *
     * @param String $table Nome da tabela.
     * @param Mixed[] $params parametros de consulta.
     *
     * @return String Query de consulta.
     */
    public function select($table, $params)
    {
        $bind = array();
        $values = array();
        foreach ($params as $key => $value) {
            array_push($bind, $key."=:".$key);
            $values[":".$key] = is_integer($value) ? (integer)$value : $value;
        }
        $bind = implode($bind, ', ');
        $query = "SELECT * FROM ".$table." WHERE ".$bind.';';
        return array('query'=>$query, 'values'=>$values);
    }

    /**
     * Cria query para remover um determinado item.
     *
     * @param String $table Nome da tabela.
     * @param Mixed[] $params parametros de consulta.
     *
     * @return String Query de consulta.
     */
    public function delete($table, $params)
    {
        $bind = array();
        $values = array();
        //abc=>def
        foreach ($params as $key => $value) {
            // Adciciona o parametro atual à lista de parametros da query.
            array_push($bind, $key."=:".$key);
            $values[":".$key] = is_integer($value) ? (integer)$value : $value;
        }
        $bind = implode($bind, ', ');
        $query = "DELETE FROM ".$table." WHERE ".$bind.';';
        return array('query'=>$query, 'values'=>$values);
    }

    /**
     * Cria query para Atualizar um determinado item.
     *
     * @param String $table Nome da tabela.
     * @param Mixed[] $params parametros de consulta.
     *
     * @return String Query de consulta.
     */
    public function update($table, $params, $id)
    {
        //Cria array para receber os parametros de entrada da query.
        $objs = array();
        // Cria array para receber os valores que serão executados.
        $values = array();
        // Varre os parametros recebidos.
        foreach ($params as $obj) {
            // Adciciona o parametro atual à lista de parametros da query.
            array_push($objs, $obj['column']. "= :".$obj['column']);
            $values[":".$obj['column']] = $obj['value'];
        }
        // Implode o array de parametros.
        $objs = implode($objs, ', ');
        // Cria a query.
        $query = "UPDATE ".$table." SET ".$objs." WHERE id=" . $id;
        // Retorna a query e os dados necessários para executala.
        return array('query'=>$query, 'values'=>$values);
    }
}
