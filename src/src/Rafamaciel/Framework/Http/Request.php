<?php

namespace Rafamaciel\Framework\Http;

use Rafamaciel\Framework\Routing\Routing;

/**
 * Trata as requisições
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br>
 * 
 * @uses Rafamaciel\Framework\Routing\Routing
 */
class Request
{

    /**
     * @var String $method Armazena o metodo atual.
     */
    private $method;

    /**
     * @var Routing $routing Manipula as rotas.
     */
    protected $routing;

    /**
     * @var ObjectArray $params Armazenas os parametros passados pela URL.
     */
    private $params;

    /**
     * Construtor da classe.
     */
    public function __construct()
    {
        // instancia o metodo da requisição.
        $this->method = $_SERVER['REQUEST_METHOD'];
        // instancia o objeto de manipulação de rotas.
        $this->routing = new Routing();
    }

    /**
     * Obtem a URL atual.
     */
    public function proccess()
    {
        // obtem os dados da rota.
        $route = $this->routing->check($_SERVER['REQUEST_URI']);
        $realPath = null;
        foreach ($route as $key => $value) {
            $module = explode("::", $route[$key]['controller']);
            $realPath = $route[$key]['path'];
        }
        $this->extractArgs($realPath);
        // Define o controller que irá processar a rota.
        $controller = new $module[0]();
        // Define o ação que será processada.
        $action = $module[1] . "Action";
        // Executa a ação.
        $controller->$action();
    }

    /**
     * Extrai os parametros passados pela url.
     *
     * @param String $realPath Caminho real da rota.
     */
    public function extractArgs($realPath)
    {
        $result = array();
        $actualPath = explode('/', $_SERVER['REQUEST_URI']);
        $realPath = explode('/', $realPath);
        for ($i =0; $i<count($realPath); $i++) {
            if ($realPath[$i] == '@var') {
                array_push($result, $actualPath[$i]);
            }
        }
        $this->params = $result;
    }

    /**
     * Obtem o metodo atual (GET, POST, PUT, DELETE)
     *
     * @return String Metodo utilizado na requisição.
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Obtem os argumentos passados na URL.
     *
     * @return Array Argumentos extraidos da URL.
     */
    public function getArgs()
    {
        $route = $this->routing->check($_SERVER['REQUEST_URI']);
        $realPath = null;
        foreach ($route as $key => $value) {
            $module = explode("::", $route[$key]['controller']);
            $realPath = $route[$key]['path'];
        }
        $this->extractArgs($realPath);
        return $this->params;
    }
}
