<?php

namespace Rafamaciel\Framework\Model;

use \ReflectionClass;
use \ReflectionProperty;
use \ReflectionMethod;
use \Exception;

/**
 * Base dos Modelos
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class Model
{
    /**
     * @var string $table Nome da tabela.
     */
    private $table;

    /**
     * Construtor da classe
     */
    public function __construct()
    {
        // Remove o prefixo Model (caso exista) e converte para minúsculo.
        $this->table = strtolower(str_replace('Model', '', (get_class($this))));
        // Explode o namespace.
        $this->table = explode("\\", $this->table);
        // Obtem apenas o fim do name space.
        $this->table = end($this->table);
    }

    /**
     * Define o nome da tabela manualmente
     *
     * @param string $table Nome da tabela.
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Recupera o nome da Tabela.
     *
     * @return String Nome da tabela.
     */
    public function getTable()
    {
        return $this->table;
    }
}
