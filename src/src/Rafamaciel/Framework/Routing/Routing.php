<?php

namespace Rafamaciel\Framework\Routing;

use Rafamaciel\Framework\Yaml\Yaml;
use \Exception;

/**
 * Realiza a manipulação das rotas.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @uses Rafamaciel\Framework\Yaml\Yaml;
 */
class Routing
{
    /**
     * @var Mixed[] $routes Armazena as rotas da aplicação.
     */
    private $routes;

    /**
     * @var String $routeFile Armazena o caminho do arquivo de rotas.
     */
    private $routeFile;

    /**
     * @var Mixed[] $paths Armazena todos os caminhos de rotas.
     */
    private $paths = array();

    /**
     * Construtor da classe
     *
     * @param String|null Contem o caminho para um arquivo de rotas personalizado.
     * @uses Rafamaciel\Framework\Yaml\Yaml;
     */
    public function __construct($customFile = null)
    {
        // verifica se o usuário enviou um arquivo de rotas personalizado, se não, utiliza o caminho padrão.
        $this->routeFile = ($customFile != null) ? $customFile : ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR .'routing.yml');
        // Faz o carregamento das rotas.
        $this->load();
    }

    /**
     * Carrega todas as rotas disponiveis.
     */
    private function load()
    {
        // instancia o array de rotas com os dados contidos no arquivo yml
        $routes = new Yaml($this->routeFile);
        $this->routes = $routes->get();
        // varre o array que contem as rotas em busca de seus caminhos.
        foreach ($this->routes as $key => $value) {
            // adiciona o caminho na lista de caminhos.
            array_push($this->paths, $value['path']);
        }
    }

    /**
     * Recupera uma rota pelo nome
     *
     * @param String $routeName Nome da rota.
     * @return String Caminho para a rota.
     */
    public function path($routeName)
    {
        // verifica se o arquivo de rotas não contem a rota desejada.
        if (!isset($this->routes[$routeName]['path'])) {
            // cria a mensagem de erro.
            throw new Exception('Impossivel recuperar o caminho para a rota ' . $routeName . '.');
        } else {
            // retorna a rota desejada.
            return $this->routes[$routeName]['path'];
        }
    }

    /**
     * Altera o arquivo de rotas manualmente.
     *
     * @param String $file Novo caminho do arquivo de rotas.
     */
    public function changeFile($file)
    {
        // altera o caminho do arquivo de rotas.
        $this->routeFile = $file;
        // recarrega o array contando as rotas com os dados do novo arquivo de rotas.
        $this->load();
    }

    /**
     * Adiciona uma nova rota.
     *
     * @param Mixed[] $route Array contendo a nova rota.
     */
    public function add($route)
    {
        // varre o array recebida.
        foreach ($route as $key => $value) {
            // adiciona a rota a lista de rotas.
            $this->routes[$key]=$value;
            // adiciona os dados da rota.
            array_push($this->paths, $value['path']);
        }
    }

    /**
     * Exibe todos os caminhos de rotas registrados.
     *
     * @return Mixed[] Array com todas as rotas.
     */
    public function getPaths()
    {
        return $this->paths;
    }

    /**
     * Verifica se uma rota existe através de seu caminho.
     *
     * @param String $route Caminho da rota desejada.
     * @return Mixed[]|null Array contendo dados da rota ou null caso a rota não exista.
     */
    public function check($route)
    {
        // verifica se a rota existe.
        if ($this->checkRoute($route) == null) {
            throw new Exception('A rota desejada ' . $route . ' não existe.');
        } else {
            return $this->getRouteByPath($this->checkRoute($route));
        }
    }

    /**
     * Obtem os dados de uma rota pelo seu path.
     *
     * @param String $route Caminho desejado.
     * @return Mixed[]|null Array contendo os dados da rota ou null caso a rota não exista.
     */
    private function getRouteByPath($route)
    {
        // varre a lista de rotas em busca da rota desejada.
        foreach ($this->routes as $key => $value) {
            // compara a rota atual com a rota desejada.
            if ($value['path'] == $route) {
                // retorna a atual.
                return array($key=>$value);
            }
        }
        // caso não encontre, retorna null.
        return null;
    }

    /**
     * Verifica se um path existe.
     *
     * @param String $route Caminho a ser verificado.
     * @return String|null Caminho real da rota ou null caso a rota não exista.
     */
    private function checkRoute($route)
    {
        $similars = array();
        // Transforma a rota em um array;
        $routeArray = explode('/', $route);
        // Obtem o tamanho da rota.
        $routeSize = count($routeArray);
        // Explode os paths e compara o tamanho da rota.
        foreach ($this->paths as $key => $value) {
            // Obtem o tamanho da rota atual.
            $temp = explode('/', $value);
            // Compara com o tamanho da rota que esta sendo verificada.
            if (count($temp) == $routeSize) {
                // Adiciona a rota atual a lista de provaveis rotas.
                array_push($similars, $value);
            }
        }
        // caso a lista de similares esteja vazia, retorna null.
        if (count($similars)==0) {
            return null;
        }
        // verifica se é uma rota estática (Exemplo: /produtos);
        foreach ($similars as $key => $value) {
            if ($value == $route) {
                // retorna a rota.
                return $value;
            }
        }
        $clone = $similars;
        // varifica se a rota é dinamica
        foreach ($similars as $key => $value) {
            // transforma a rota atual em array.
            $temp = explode('/', $value);
            //varre a rota atual substituindo as variaveis.
            foreach ($temp as $campo => $valor) {
                if ($valor == "@var") {
                    $temp[$campo] = $routeArray[$campo];
                }
                // Transforma o resultado em uma string.
                $result = implode("/", $temp);
            }
            // Atualiza a lista de similares.
            $similars[$key] = $result;
        }
        // Verfica se a rota existe.
        foreach ($similars as $key => $value) {
            if ($value == $route) {
                // retorna a rota.
                return $clone[$key];
            }
        }
        // return null
        return null;
    }
}
