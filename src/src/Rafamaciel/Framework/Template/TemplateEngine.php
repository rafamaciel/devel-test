<?php

namespace Rafamaciel\Framework\Template;

/**
 * Base do Controller
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 */
class TemplateEngine
{
    /**
     * @var Array $data Armazena os dados que serão renderizados.
     */
    private $data = array();
    
    /**
     * @var Array $dataView Reescrita do $data.
     */
    private $dataView = array();

    /**
     * @var String $directory Diretorio que contem os arquivos das views.
     */
    private $directory;

    /**
     * Construtor da classe
     *
     * @param String $directory caminho para o diretorio das views.
     */
    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    /** 
     * Renderiza as views
     *
     * @param String $fileName Nome do arquivo que será renderizado.
     * @param Array|bool Dados que serão renderizados.
     */
    public function render($fileName, $data = false)
    {
        if ($data) {
            $this->dataView = $data;
        }
        $fileName = $this->directory . $fileName . '.html';
        $rendered = "";
        if (file_exists($fileName)) {
            ob_start();
            require ($fileName);
            $rendered = ob_get_contents();
            ob_end_clean();
        }
        $this->dataView = array();
        echo $rendered;
    }

    /** 
     * Renderiza um elemento atráves de foreach.
     *
     * @param String $fileName Nome do arquivo que será renderizado.
     * @param Array $dataAr Array contendo os dados que serão renderizados.
     *
     * @return String View renderizada.
     */
    public function renderAr ($fileName, $dataAr)
    {
        $rendered = "";
        if (count($dataAr && is_array($dataAr))) {
            foreach ($dataAr as $data) {
                $rendered.= $this->render($fileName, $data);
            }
        }
        return $rendered;
    
    }

    /**
     * Magic set (Define atributos ao template)
     *
     * @param String $key Nome do atributo.
     * @param Mixed[] $value Valor do atributo.
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
    }
    
    /**
     * Magic get (Recupera atributos no template)
     *
     * @param String $key nome do atributo.
     *
     * @return Mixed[] Valor do atributo.
     */
    public function __get($key)
    {
        if (isset($this->dataView[$key])) {
            return $this->dataView[$key];
        } else if (isset($this->data[$key])) {
            return $this->data[$key];
        } else {
            return false;
        }
    }
}
