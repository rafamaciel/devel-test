<?php

namespace Rafamaciel\Framework\Yaml;

use \Exception;

/**
 * Realiza a manipulação dos arquivos yaml.
 *
 * @package Framework
 * @author Rafael B. Maciel <rafael@haxor.com.br> 
 *
 * @todo Adicionar método setFile para alterar o arquivo yml
 * @todo Adicionar método revertLiteralPlaceHolder
 * @todo Adicionar método inlineEscape
 */
class Yaml
{
    /**
     * @var string $file.
     */
    protected $file;

    /**
     * @var string $path
     */
    protected $path = array();

    /**
     * @var array $aliases;
     */
    protected $aliases = array();

    /**
     * @var array $anchors;
     */
    protected $anchors = array();

    /**
     * @var array $result;
     */
    protected $result = array();

    /**
     * @var array $delay;
     */
    protected $delay = array();

    /**
     * Construtor da classe.
     *
     * @param string $file
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Carrega o arquivo yml
     *
     * @param string $file file to path
     */
    private function load()
    {
        if (!file_exists($this->file)) {
            throw new Exception('O Arquivo Yaml '.$this->file.'não existe.');
        }
        $this->toString(file($this->file));
    }

    /**
     * Recupera o array com os valores do arquivo Yml
     *
     * @return Array Array contendo os dados do arquivo yml;
     */
    public function get($attr = null)
    {
        $this->load();
        if ($attr == null) {
            return $this->result;
        } else if (!isset($this->result[$attr])) {
            throw new Exception('O Atributo '.$attr.' não está configurado em '.$this->file);
        } else {
            return $this->result[$attr];
        }
    }

    /**
     * Converte o conteudo do arquivo para string.
     *
     * @param File $lines Conteudo do arquivo.
     *
     */
    private function toString($lines)
    {
        $count = count($lines);
        for ($i=0; $i<$count; ++$i) {
            $line = $lines[$i];
            $indent = $idt = strlen($line) - strlen(ltrim($line));
            if ($indent === 0) {
                $tmpPath = array();
            } else {
                $tmpPath = $this->path;
                do {
                    end($tmpPath);
                    if ($indent<=($lastIndentPath=key($tmpPath))) {
                        unset($tmpPath[$lastIndentPath]);
                    }
                } while ($indent<=$lastIndentPath);
            }
            if ($indent === -1) {
                $idt = strlen($line) - strlen(ltrim($line));
            }
            $line = substr($line, $idt);
            $tmpLine = trim($line);
            if ($tmpLine === '' || $line[0] === '#' || trim($line, " \r\n\t") === '---') {
                continue;
            }
            $this->path = $tmpPath;
            $lastChar = substr($tmpLine, -1);
            $literalBlockStyle = (($lastChar !== '>' && $lastChar !== '|') || preg_match('#<.*?>$#', $line)) ? false : $lastChar;
            if ($literalBlockStyle) {
                $literalBlock = '';
                $line = rtrim($line, $literalBlockStyle . " \n") . '__YAML__';
                $literalBlockIndent = strlen($lines[++$i]) - strlen(ltrim($lines[$i--]));
                while (++$i < $count && (! trim($lines[$i]) || (strlen($lines[$i]) - strlen(ltrim($lines[$i]))) > $indent)) {
                    $tmpLine = $lines[$i];
                    $tmpLineTrimedDistance = strlen($tmpLine) - strlen(ltrim($tmpLine));
                    $tmpLine = substr($tmpLine, ($literalBlockIndent === -1) ? $tmpLineTrimedDistance : $literalBlockIndent);
                    if ($literalBlockStyle !== '|') {
                        $tmpLine = substr($tmpLine, $tmpLineTrimedDistance);
                    }
                    $tmpLine = rtrim($tmpLine, "\r\n\t ") . "\n";
                    if ($literalBlockStyle === '|') {
                        $literalBlock .= $tmpLine;
                    } else if ($tmpLine == "\n" && $literalBlockStyle === '>') {
                        $literalBlock = rtrim($literalBlock, " \t") . "\n";
                    } else if (strlen($tmpLine) === 0) {
                        $literalBlock = rtrim($literalBlock, ' ') . "\n";
                    } else {
                        if ($tmpLine !== "\n") {
                            $tmpLine = trim($tmpLine, "\r\n ") . " ";
                        }
                        $literalBlock .= $tmpLine;
                    }
                }
                --$i;
            }
            while (++$i < $count) {
                $tmpLine = trim($line);
                if (!strlen($tmpLine) || substr($tmpLine, -1, 1) === ']') {
                    break;
                }
                if ($tmpLine[0] === '[' || preg_match('#^[^:]+?:\s*\[#', $tmpLine)) {
                    $line = rtrim($line, " \n\t\r") . ' ' . ltrim($lines[$i], " \t");
                    continue;
                }
                break;
            }
            --$i;
            if (strpos($line, '#') && strpos($line, '"') === false && strpos($line, '\'') === false) {
                $line = preg_replace('/\s+#(.+)$/', '', $line);
            }
            $lineArray = (!$line || ! ($line = trim($line))) ? array() : $this->parseLine($line, $indent);
            if ($literalBlockStyle) {
                $lineArray = $this->revertLiteralPlaceHolder($lineArray, $literalBlock);
            }
            if (count($lineArray) > 1) {
                $groupPath = $this->$path;
                foreach ($lineArray as $k => $v) {
                    $this->addArray(array($k => $v), $indent);
                    $this->$path = $groupPath;
                }
            } else {
                $this->addArray($lineArray, $indent);
            }
            foreach ($this->delay as $idt => $delayedPath) {
                $this->$path[$idt] = $delayedPath;
            }
            $this->delay = array();
        }
            return $this->result;
    }

    /**
     * Converte a linha para array
     *
     * @param String $line Linha que será convertida.
     * @param Integer $ident Identação da linha.
     *
     * @return Array Resultado da conversão.
     */
    private function parseLine($line, $indent)
    {
        $ret = array();
        if ((($line[0] === '&' || $line[0] === '*') && preg_match('/^(&[A-z0-9_\-]+|\*[A-z0-9_\-]+)/', $line, $match)) || preg_match('/(&[A-z0-9_\-]+|\*[A-z0-9_\-]+)$/', $line, $match) || preg_match ('#^\s*<<\s*:\s*(\*[^\s]+).*$#', $line, $match)) {
            if ($match[1][0] === '&') {
                $this->anchors = substr($match[1], 1);
            } else if ($match[1][0] === '*') {
                $this->aliases = substr($match[1], 1);
            }
            $line = trim(str_replace($match[1], '', $line));
        }
        $last = substr($line, -1, 1);
        if ($line[0] === '-' && $last === ':') {
            if (($key = trim(substr($line, 1, -1)))) {
                if ($key[0] === '\'') {
                    $key = trim($key, '\'');
                }
                if ($key[0] === '"') {
                    $key = trim($key, '"');
                }
            }
            $ret[$key] = array();
            $this->delay = array((strpos($line, $key) + $indent) => $key);
            return array($ret);
        }
        if ($last === ':') {
            if (($key = trim(substr($line, 0, -1)))) {
                if ($key[0] === '\'') {
                    $key = trim($key, '\'');
                }
                if ($key[0] === '"') {
                    $key = trim($key, '"');
                }
            }
            $ret[$key] = '';
            return $ret;
        }
        if ($line && $line[0] === '-' && ! (($tmpLen = strlen($line)) > 3 && substr($line, 0, 3) === '---')) {
            if ($tmpLen <= 1) {
                $ret = array(array());
            } else {
                $ret[] = $this->toType(trim(substr($line, 1)));
            }
            return $ret;
        }
        if ($line[0] === '[' && $last === ']') {
            return $this->toType($line);
        }
        if (strpos($line, ':')) {
            if (($line[0] === '"' || $line[0] === '\'') && preg_match('#^(["\'](.*)["\'](\s)*:)#', $line, $match)) {
                $val = trim(str_replace($match[1], '', $line));
                $key = $match[2];
            } else {
                $point = strpos($line, ':');
                $key = trim(substr($line, 0, $point));
                $val = trim(substr($line, ++$point));
            }
            if ($key === '0') {
                $key = '__ZERO__';
            }
            $ret[$key] = $this->toType($val);
        } else {
            $ret = array($line);
        }
            return $ret;
    }

    /**
     * Adiciona o array ao grupo
     *
     * @param Array $lineArray Array que será adicionado ao grupo.
     * @param Integer $ident Identação da linha.
     */
    private function addArray($lineArray, $indent)
    {
        $key = key($lineArray);
        $val = (isset($lineArray[$key])) ? $lineArray[$key] : null;
        if ($key === '__ZERO__') {
            $key = '0';
        }
        if ($indent === 0 && ! $this->aliases && ! $this->anchors) {
            if ($key || $key === '' || $key === '0') {
                $this->result[$key] = $val;
            } else {
                $this->result[] = $val;
                end($this->result);
                $key = key($this->result);
            }
            $this->path[$indent] = $key;
            return;
        }
        $history[] = $_arr = $this->result;
        foreach ($this->path as $path) {
            $history[] = $_arr = $_arr[$path];
        }
        if ($this->aliases) {
            if (! isset($this->$savedGroups[$this->aliases])) {
                throw new LogicException('Bad group name:' . $this->aliases . '.');
            }
            $val = $this->result;
            foreach ($this->$savedGroups[$this->aliases] as $g) {
                $val = $val[$g];
            }
            $this->aliases = false;
        }
        if ((string)$key === $key && $key === '<<') {
            $_arr = (is_array($_arr)) ? array_merge($_arr, $val) : $val;
        } else if ($key || $key === '' || $key === '0') {
            if (! is_array($_arr)) {
                $_arr = array($key => $val);
            } else {
                $_arr[$key] = $val;
            }
        } else {
            if (! is_array($_arr)) {
                $_arr = array($val);
                $key = 0;
            } else {
                $_arr[] = $val;
                end($_arr);
                $key = key($_arr);
            }
        }
        $reversePath = array_reverse($this->path);
        $reverseHistory = array_reverse($history);
        $reverseHistory[0] = $_arr;
        $count = count($reverseHistory) - 1;
        for ($i = 0; $i < $count; ++$i) {
            $reverseHistory[$i + 1][$reversePath[$i]] = $reverseHistory[$i];
        }
        $this->result = $reverseHistory[$count];
        $this->path[$indent] = $key;
        if ($this->anchors) {
            $this->$savedGroups[$this->anchors] = $this->path;
            if (is_array($val)) {
                $k = key($val);
                if ((int)$k !== $k/*! is_int($k)*/) {
                    $this->$savedGroups[$this->anchors][$indent + 2] = $k;
                }
            }
            $this->anchors = false;
        }
    }

    /**
     * Transforma o conteudo recebido no formato correto.
     *
     * @param String $str Linha do arquivo.
     *
     * @return Mixed[] Resultado da conversão.
     */
    private function toType($str)
    {
        if ($str === '') {
            return null;
        }
        $first = $str[0];
        $last = substr($str, -1, 1);
        $isQuoted = false;
        do {
            if (! $str || ($first !== '"' && $first !== '\'') || ($last !== '"' && $last !== '\'')) {
                break;
            }
            $isQuoted = true;
        } while (0);
        if ($isQuoted === true) {
            return strtr(substr($str, 1, -1), array('\\"' => '"', '\'\'' => '\'', '\\\'' => '\''));
        }
        if (strpos($str, ' #') !== false && $isQuoted === false) {
            $str = preg_replace('/\s+#(.+)$/', '', $str);
        }
        if ($isQuoted === false) {
            $str = str_replace('\n', "\n", $str);
        }
        if ($first === '[' && $last === ']') {
            if (($inner = trim(substr($str, 1, -1))) === '') {
                return array();
            }
            $ret = array();
            foreach ($this->inlineEscape($inner) as $v) {
                $ret[] = $this->toType($v);
            }
            return $ret;
        }
        if (($point = strpos($str, ': ')) !== false && $first !== '{') {
            return array(trim(substr($str, 0, $point)) => $this->toType(trim(substr($str, ++$point))));
        }
        if ($first === '{' && $last === '}') {
            if (($inner = trim(substr($str, 1, -1))) === '') {
                return array();
            }
            $ret = array();
            foreach ($this->inlineEscape($inner) as $v) {
                $sub = $this->toType($v);
                if (empty($sub)) {
                    continue;
                }
                if (is_array($sub)) {
                    $k = key($sub);
                    $ret[$k] = $sub[$k];
                    continue;
                }
                $ret[] = $sub;
            }
            return $ret;
        }
        if ($str === 'null' || $str === 'null' || $str === 'null' || $str === '' || $str === '~') {
            return null;
        }
        if (is_numeric($str)) {
            if ($str === '0') {
                $str = 0;
            } else if (preg_match('/^(-|)[1-9]+[0-9]*$/', $str)) {
                if (($int = (int)$str) != PHP_INT_MAX) {
                    $str = $int;
                }
            } else if (rtrim($str, 0) === $str) {
                $str = (float)$str;
            }
            return $str;
        }
        $lower = strtolower($str);
        if (in_array($lower, array('true', 'on', '+', 'yes', 'y'))) {
            return true;
        }
        if (in_array($lower, array('false', 'off', '-', 'no', 'n'))) {
            return false;
        }
        return $str;
    }
}
