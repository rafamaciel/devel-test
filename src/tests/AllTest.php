<?php
// Obtem o autoload do composer.
require "../vendor/autoload.php";

use Rafamaciel\Framework\Yaml\Yaml;
use Rafamaciel\Framework\Routing\Routing;
use Rafamaciel\Framework\Model\Model;

class AllTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * Verifica se o nome da classe foi reescrito no construtor.
     */
    public function testModelConstructor()
    {
        $model = new Model();
        $this->assertEquals("", $model->getTable());
    }

    /**
     * @test
     * Verifica se os metodos Get e Set da tabela estão funcionando corretamente.
     */
    public function testeModelGetSet()
    {
        $model = new Rafamaciel\Framework\Model\Model();
        $model->setTable("tabela");
        $this->assertEquals("tabela", $model->getTable());
    }

    /**
     * @test
     * Testa a recuperação de informações no arquivo yml.
     */
    public function testYmlGet()
    {
        // Seleciona o arquivo de teste.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'config.yml';
        // Inicia a classe.
        $yaml = new Yaml($file);
        // Obtem um determinado item do arquivo yml.
        $yaml = $yaml->get('test');
        // Realiza o teste.
        $this->assertArrayHasKey('name', $yaml);
    }

    /**
     * @test
     * Obtem o path da rota através de seu nome.
     */
    public function testRoutingPath()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza Teste
        $this->assertEquals('/add', $routing->path('add_post'));
    }

    /**
     * @test
     * Troca o arquivo de rotas.
     */
    public function testRoutingChangeFile()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza Teste
        $this->assertEquals('/add', $routing->path('add_post'));
        // Define o novo arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing2.yml';
        //Realiza a troca do arquivo de rotas.
        $routing->changeFile($file);
        // Realiza o teste
        $this->assertEquals('/add', $routing->path('add_item'));

    }

    /**
     * @test
     * Obtem todos os paths registrados.
     */
    public function testRoutingGetPaths()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Obtem a lista de paths.
        $paths = $routing->getPaths();
        // Verifica se o path /show/@var está na lista.
        $this->assertContains('/show/@var', $paths);
    }

    /**
     * @test
     * Obtem todos os paths registrados.
     */
    public function testRoutingCheck()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza o teste com uma rota existente.
        $this->assertArrayHasKey('add_post', $routing->check('/add'));
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function testRoutingCheckException()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza o teste com uma rota existente.
        $this->assertArrayHasKey('add_post', $routing->check('/inexist'));
    }
}
