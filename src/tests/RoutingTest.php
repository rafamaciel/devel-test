<?php

require "../vendor/autoload.php";

use Rafamaciel\Framework\Routing\Routing;

class RoutingTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     * Obtem o path da rota através de seu nome.
     */
    public function testPath()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza Teste
        $this->assertEquals('/add', $routing->path('add_post'));
    }

    /**
     * @test
     * Troca o arquivo de rotas.
     */
    public function testChangeFile()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza Teste
        $this->assertEquals('/add', $routing->path('add_post'));
        // Define o novo arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing2.yml';
        //Realiza a troca do arquivo de rotas.
        $routing->changeFile($file);
        // Realiza o teste
        $this->assertEquals('/add', $routing->path('add_item'));

    }

    /**
     * @test
     * Obtem todos os paths registrados.
     */
    public function testGetPaths()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Obtem a lista de paths.
        $paths = $routing->getPaths();
        // Verifica se o path /show/@var está na lista.
        $this->assertContains('/show/@var', $paths);
    }

    /**
     * @test
     * Obtem todos os paths registrados.
     */
    public function testCheck()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza o teste com uma rota existente.
        $this->assertArrayHasKey('add_post', $routing->check('/add'));
    }

    /**
     * @test
     * @expectedException Exception
     */
    public function testCheckException()
    {
        // Define o arquivo de rotas.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'routing.yml';
        // Inicia o objeto.
        $routing = new Routing($file);
        // Realiza o teste com uma rota existente.
        $this->assertArrayHasKey('add_post', $routing->check('/inexist'));
    }
}
