<?php

require "../vendor/autoload.php";

use Rafamaciel\Framework\Yaml\Yaml;

class YamlTest extends PHPUnit_Framework_TestCase
{

    /**
     * @test
     * Testa a recuperação de informações no arquivo yml.
     */
    public function testGet()
    {
        // Seleciona o arquivo de teste.
        $file = __DIR__ . DIRECTORY_SEPARATOR . 'config.yml';
        // Inicia a classe.
        $yaml = new Yaml($file);
        // Obtem um determinado item do arquivo yml.
        $yaml = $yaml->get('test');
        // Realiza o teste.
        $this->assertArrayHasKey('name', $yaml);
    }
}
